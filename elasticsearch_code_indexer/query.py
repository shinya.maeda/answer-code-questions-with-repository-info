from elasticsearch import Elasticsearch
import os
from anthropic import Anthropic

ELASTICSEARCH_INDEX_NAME = 'code'
ELASTICSEARCH_QUERY_RESULT_SIZE = 5

####################### Try your query #######################
query = 'Which class should I use to create a new CI pipeline?'
##############################################################

client = Elasticsearch(
    'https://localhost:9200',
    ca_certs='http_ca.crt',
    api_key=os.environ.get("ELASTICSEARCH_API_KEY")
)

print(client.info())

resp = client.search(
    index=ELASTICSEARCH_INDEX_NAME,
    body=
    {
        "query": {
            "match": {
                "content": query
            }
        },
        "size": ELASTICSEARCH_QUERY_RESULT_SIZE
    },
)
print(f"total: {resp['hits']['total']}")
print(f"max_score: {resp['hits']['max_score']}")
print(f"total hits: {len(resp['hits']['hits'])}")

# See https://docs.anthropic.com/claude/docs/how-to-use-system-prompts
documents_for_prompt = ""
documents_for_prompt += "<documents>\n"

for idx, hit in enumerate(resp['hits']['hits']):
    documents_for_prompt += f"<document index=\"{idx+1}\">\n"
    documents_for_prompt += "<source>\n"
    documents_for_prompt += hit['_source']['path'] + "\n"
    documents_for_prompt += "</source>\n"
    documents_for_prompt += "<document_content>\n"
    documents_for_prompt += hit['_source']['content'] + "\n"
    documents_for_prompt += "</document_content>\n"
    documents_for_prompt += "</document>\n"

documents_for_prompt += "</documents>\n"

# print(documents_for_prompt)

client = Anthropic(
    # This is the default and can be omitted
    api_key=os.environ.get("ANTHROPIC_API_KEY"),
)

system_prompt = """
Here are some documents for you to reference for your task:

{documents_for_prompt}

You are a helpful coding assistant.
You only answer questions about coding and nothing else.
Use the provided documents to answer to the user's questions.
""".format(documents_for_prompt=documents_for_prompt)

stream = client.messages.create(
    max_tokens=1024,
    messages=[
        {
            "role": "user",
            "content": query,
        }
    ],
    system=system_prompt,
    model="claude-2.1",
    stream=True,
)
for event in stream:
    if event.type == "content_block_delta":
        print(event.delta.text, end="", flush=True)
