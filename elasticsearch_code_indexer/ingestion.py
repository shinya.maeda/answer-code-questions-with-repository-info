import subprocess
import os
from urllib.parse import urlparse

REPO_URL = os.environ.get("REPO_URL")
EXTENSIONS = os.environ.get("FILE_EXTENSIONS")
ELASTICSEARCH_INDEX_NAME = 'code'

parsed_url = urlparse(REPO_URL)
zip_file_name = os.path.basename(parsed_url.path)

print("Downloading repository")

subprocess.run(['rm', '-Rf', 'repo'])
subprocess.run(['mkdir', '-p', 'repo'])
subprocess.run(['wget', '-P', 'repo', REPO_URL])
subprocess.run(['unzip', f"repo/{zip_file_name}", "-d", "repo"])
subprocess.run(['rm', '-Rf', f"repo/{zip_file_name}"])

print("Extracting repository for Elasticsearch bulk inserts")

from elasticsearch import Elasticsearch

client = Elasticsearch(
    'https://localhost:9200',
    ca_certs='http_ca.crt',
    api_key=os.environ.get("ELASTICSEARCH_API_KEY")
)

print(client.info())

try:
    client.indices.delete(index=ELASTICSEARCH_INDEX_NAME)
except:
    pass

client.indices.create(index=ELASTICSEARCH_INDEX_NAME)

documents = []

for root, dirnames, filenames in os.walk('repo'):
    for filename in filenames:
        if filename.endswith(EXTENSIONS):
            path = os.path.join(root, filename)

            with open(path, 'r') as f:
                header = { "index" : { "_index" : ELASTICSEARCH_INDEX_NAME } }
                documents.append(header)
                document = {"content": f.read(), "path": path}
                documents.append(document)

    if len(documents) > 0:
        resp = client.bulk(body=documents)
        print(resp)
        documents = []
