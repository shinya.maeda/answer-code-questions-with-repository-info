# Answer Code Questions with repository information

This project demonstrates a RAG pipeline that generates AI-powered answer with retrieving repository/code information.

- Use Elasticsearch for BM25 keyword search.
- Use Anthropic for Code Generation.
- No semantic search and embeddings are used.

```mermaid
flowchart LR
  subgraph RAG pipeline
    direction LR
    subgraph User
      Question["Question"]
    end

    subgraph Elasticsearch
        Code["Code"]
    end

    subgraph Anthropic["Anthropic"]
        Claude(["Claude 2.1"])
    end
  end

  Question -- BM25 Keyword search --> Code
  Code -- Feed --> Claude
  Claude -- Answer --> User
```

See [this demo video](https://youtu.be/2Ub70Ow8yag?feature=shared) for more information.

## Setup

1. Clone this repo and run `poetry install`.
1. Follow [Quick start](https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started.html) to setup a self-managed Elasticsearch instance in your local machine.
1. Make sure `curl --cacert http_ca.crt -u elastic:$ELASTIC_PASSWORD https://localhost:9200` succeeds.
1. Set the following environment variables:

```
# Set your favorite repository
export REPO_URL="https://gitlab.com/gitlab-org/gitlab/-/archive/master/gitlab-master.zip"
export FILE_EXTENSIONS="rb"

# See https://www.elastic.co/guide/en/kibana/current/api-keys.html about how to create an API key
export ELASTICSEARCH_API_KEY="S1NJbHo0MEI1czB4eXhsNjJ2YWY6eUZZaXJ4dUZRTzZfTGhFSThObldrZw=="

export ANTHROPIC_API_KEY=<your-anthropic-api-key>
```

1. Run `poetry run python elasticsearch_code_indexer/ingestion.py` to ingest the data from repository URL to Elasticsearch.

## Generate an AI-powered answer

1. Set your query in `elasticsearch_code_indexer/query.py`
1. Run `poetry run python elasticsearch_code_indexer/query.py` to answer user's code related questions.

## References

- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142787
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145343